<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/firstrate/admin/');
define('HTTP_CATALOG', 'http://localhost/firstrate/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/firstrate/admin/');
define('HTTPS_CATALOG', 'http://localhost/firstrate/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/firstrate/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/firstrate/system/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/firstrate/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/firstrate/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/firstrate/system/config/');
define('DIR_IMAGE', 'C:/xampp/htdocs/firstrate/image/');
define('DIR_CACHE', 'C:/xampp/htdocs/firstrate/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/firstrate/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/firstrate/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/firstrate/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/firstrate/system/storage/upload/');
define('DIR_CATALOG', 'C:/xampp/htdocs/firstrate/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'dev#9347');
define('DB_DATABASE', 'firstrate');
define('DB_PORT', '3306');
define('DB_PREFIX', '');
